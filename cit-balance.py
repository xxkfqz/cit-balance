#!/usr/bin/env python3
# </> with <3 xxkfqz <xxkfqz@gmail.com> 2d19-2d20

import sys
import argparse

try:
    import requests
    from bs4 import BeautifulSoup
except ImportError as e:
    print('Module "{}" not found'.format(e.name))
    sys.exit(-1)

utm_address = 'http://10.0.0.7'
post_http_header = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9',
    'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
    'Accept-Encoding': 'gzip, deflate',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Content-Length': '160',
    'Origin': utm_address,
    'DNT': '1',
    'Connection': 'keep-alive',
    'Referer': utm_address,
    'Upgrade-Insecure-Requests': '1'
}

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--username', required=True)
    parser.add_argument('-p', '--password', required=True)

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(-1)
    args = parser.parse_args()

    with requests.session() as session:
        res = session.get(utm_address)
        if res.status_code != 200:
            sys.exit('Login page on {} returns {}'.format(utm_address, res.status_code))

        post_http_header['Cookie'] = 'PHPSESSID={}'.format(res.cookies['PHPSESSID'])
        payload = {
            'bootstrap[username]': args.username,
            'bootstrap[password]': args.password,
            'bootstrap[send]': '<i+class="icon-ok"></i>Войти'
        }

        r = session.post(utm_address, headers=post_http_header, data=payload)
        if r.status_code != 200:
            sys.exit('Cannot login. Status: {}'.format(r.status_code))

        bs = BeautifulSoup(r.text, 'html.parser')
        balance = bs.findAll('span', class_='badge badge-success')
        output_raw = balance[0].text.split(' руб.')[0]
        print(output_raw.replace(' ', '\n'))
